import { IMAGES } from "../constants/index";

const initialState = [];

export const imagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case IMAGES.LOAD_SUCCESS:
      return [...state, ...action.payload];

    default:
      return state;
  }
};

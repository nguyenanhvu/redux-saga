import { IMAGES } from '../constants';

export const errorReducer = (state = null, action) => {
    switch (action.type) {
        case IMAGES.LOAD_FAIL:
            return action.payload;
        case IMAGES.LOAD_SUCCESS:
            return null;
        default:
            return state;
    }
};

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadImages } from "../../actions";
import Button from "../Button/Button";
import Stats from "../Stats";

import "./styles.css";

const ImageGrid = () => {
  const dispatch = useDispatch();
  const images = useSelector((state) => state.images);
  const error = useSelector((state) => state.error);
  const isLoading = useSelector((state) => state.isLoading);
  const imageStats = useSelector((state) => state.imageStats);

  useEffect(() => {
    if (!loadImages()) return;
    dispatch(loadImages());
  }, [dispatch]);

  return (
    <div className="content">
      <section className="grid">
        {images.map((image) => (
          <div
            key={image.id}
            className={`item item-${Math.ceil(image.height / image.width)}`}
          >
            <Stats stats={imageStats[image.id]} />
            <img src={image.urls.small} alt={image.user.username} />
          </div>
        ))}
      </section>
      {error && <div className="error">{JSON.stringify(error)}</div>}
      <Button
        onClick={() => !isLoading && dispatch(loadImages())}
        loading={isLoading}
      >
        Load images
      </Button>
    </div>
  );
};

export default ImageGrid;

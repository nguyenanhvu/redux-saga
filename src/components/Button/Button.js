import React from "react";
import "./button.css";

function Button({ children, loading, ...props }) {
  return (
    <button className="button" disabled={loading} {...props}>
      {loading ? "Loading..." : children}
    </button>
  );
}

Button.defaultProps = {
  loading: false,
};

export default Button;

import { all } from "redux-saga/effects";
import imagesSaga from "./imagesSaga";
import watchStatsRequest from "./statSaga";

function* rootSagas() {
  yield all([imagesSaga(), watchStatsRequest()]);
}

export default rootSagas;

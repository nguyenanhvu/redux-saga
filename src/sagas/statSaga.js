import { call, fork, put, take } from "redux-saga/effects";
import { loadImagesStats, setErrorStats, setImagesStats } from "../actions";
import { IMAGES } from "../constants";
import { fetchImageStats } from "../api";

function* handleStatsRequest(id) {
  for (let i = 0; i < 3; i++) {
    try {
      yield put(loadImagesStats(id));
      const res = yield call(fetchImageStats, id);
      yield put(setImagesStats(id, res.downloads.total));
      // image was loaded so we exit the generator
      return true;
    } catch (e) {
      // we just need to retry and dispatch an error
      // if we tried more than 3 times
    }
  }
  yield put(setErrorStats(id));
}

function* watchStatsRequest() {
  while (true) {
    const { payload } = yield take(IMAGES.LOAD_SUCCESS);
    for (let i = 0; i < payload.length; i++) {
      yield fork(handleStatsRequest, payload[i].id);
    }
  }
}

export default watchStatsRequest;
